#pragma once

#include <boost/lockfree/spsc_queue.hpp>
#include <nanopi/mavlink.hpp>

#define MESSAGE_QUEUE_SIZE 32

namespace nanopi {

/**
 * Lockfree single producer - single consumer
 * message queue for background message passing.
 *
 * Queue capacity is fixed and may be controlled
 * via the `MESSAGE_QUEUE_SIZE` macro.
 */
class message_queue
{
private:
    boost::lockfree::spsc_queue <
        mavlink_message_t,
        boost::lockfree::capacity<1024>
    > _queue;
    mavlink_message_t _buf;
    int _eventfd;
public:
    message_queue();
    ~message_queue();
public:
    /**
     * Put the message to this queue.
     * @return `true` on success
     */
    bool push(const mavlink_message_t & msg);
    /**
     * Remove the message from this queue
     * and return. The method returns immediately
     * if queue is empty.
     * @return `true` on success
     */
    bool pop(mavlink_message_t & msg);
public:
    /**
     * Push an arbitrary message. `pack` function
     * must be specialized for type `_Msg`.
     */
    template < typename _Msg >
    bool push(const _Msg & msg)
    {
        if (!pack < _Msg > (msg, _buf)) return false;
        return push(_buf);
    }
public:
    /**
     * Wake up all waiters, if any -- signal
     * event object represented by `eventfd`.
     */
    void signal();
public:
    int get_eventfd() const { return _eventfd; }
};

}
