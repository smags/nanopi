#pragma once

#include <nlohmann/json.hpp>
#include <nlohmann/fifo_map.hpp>

template < typename K, typename V, typename dummy_compare, typename A>
using fifo_map_t = nlohmann::fifo_map < K, V, nlohmann::fifo_map_compare < K > , A > ;
using json_t = nlohmann::basic_json < fifo_map_t > ;
