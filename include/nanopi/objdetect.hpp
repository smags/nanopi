//**************************************************************
//�����������:          ����������� ������������, ���. ���� �����. ����������, �. ������ ��������    
//������:			    ��� ���							
//������ �������:       1.0                                  
//������������:         ANSI C++ 98                         
//������:   		    28.05.2019
//�������:   		    29.05.2019
//**************************************************************//

/** \author ������ �.�.*/
/** \file objdetect.hpp*/
/** \brief ����������� ����� ��� ��������� ����������� ��������� ����� � ���������� �������� ����� ��� ������� �����������.*/

#pragma once

#include <nanopi/video_capture.hpp>
#include <nanopi/serialization.hpp>

#include <opencv2/opencv.hpp>

namespace nanopi
{
	/**Object detection mode. */
	enum detection_mode
	{
		RED_BALLOONS=0,//Red balloon detecting
		WHITE_SQUARE=1 //White square detecting
	};

	/**Object detector.*/
	class objdetect
	{
	public:

		/**Detector configuration.*/
		struct config_t
		{
			/**Detection mode.*/
			detection_mode mode;

			/**Quantity of detecting objects.*/
			size_t max_objects;

			/**Low HSV ranges for red balloons.*/
			cv::Scalar color_red_low;

			/**High HSV ranges for red balloons.*/
			cv::Scalar color_red_high;

			/**Low HSV ranges for white square.*/
			cv::Scalar color_white_low;

			/**High HSV ranges for white square.*/
			cv::Scalar color_white_high;

			/**Minimal contours length.*/
			int min_contours_length;

			/**Horizontal angle of view, degree*/
			float horiz_angle;

			/**Vertical angle of view, degree.*/
			float vert_angle;

			/**Max size of red sphere, m.*/
			float red_sphere_max_size;

			/**Max size of white square, m.*/
			float white_square_max_size;

		};
	public:

		/**Structure to store center coordinates and object size in.*/
		struct obj_t
		{
			float cx, cy; //!< Coordinates of object center, degrees.
			float size;   //!< Angle size of object, degree.
			float distance; //!< Distance to object, m.
			std::vector<cv::Point> hull; //!< Hull points of the object.
		    detection_mode object_type; //!< Type of detected object.
		};

		/**Debug output container.*/
		struct debug_t
		{
			cv::Mat mask;     ///< color filter output (binary mask)
			cv::Mat debug;    ///< original frame with some debug graphics
		};

	private:

		/**Configuration parameters of detector.*/
		const config_t _config;
	
	public:

		/**Constructor.
		 *@param cfg configuration parameters.*/
		objdetect(config_t cfg);

		/**Tries to detect `config_t::max_objects` objects
		 * on input image and stores them to `res`.
		 *
		 * If `debug` is specified, debug graphics is prepared
		 * and stored to `debug`.
		 *
		 *@param frame input frame;
		 *@param res results of detection;
		 *@param debug debug data and images.
		 *@return `true` if at least one object detected.*/
		bool detect(const cv::Mat & frame, std::vector < obj_t > & res, debug_t * debug = nullptr);

		

	private:
		/**Image with detected edges.*/
		cv::Mat _detected_edges;

		/**Input image in HSV color space.*/
		cv::Mat _hsv;

		/**Binary mask after color selection.*/
		cv::Mat _mask;

		/**Debug data and images.*/
		cv::Mat _debug;

		/**Tries to detect `config_t::max_objects` objects (red balloons)
			* on input image and stores them to `res`.
			*
			* If `debug` is specified, debug graphics is prepared
			* and stored to `debug`.
			*
			*@param frame input frame;
		*@param res results of tetection;
		*@param debug debug data and images.
		*@return `true` if at least one object detected.*/
		bool detect_red_balloons(const cv::Mat & frame, std::vector < obj_t > & res, debug_t * debug = nullptr);

		/**Tries to detect `config_t::max_objects` objects (white square)
			* on input image and stores them to `res`.
			*
			* If `debug` is specified, debug graphics is prepared
			* and stored to `debug`.
			*
			*@param frame input frame;
			*@param res results of tetection;
			*@param debug debug data and images.
			*@return `true` if at least one object detected.*/
		bool detect_white_square(const cv::Mat & frame, std::vector < obj_t > & res, debug_t * debug = nullptr);

		//std::vector < cv::KeyPoint > _keypoints;
	};

	inline void to_json(json_t & j, const objdetect::config_t & c)
	{

		j = {
			{ "objects.max", c.max_objects },
			//{ "ker.dilate.size", c.dilate_ker_size },
			//{ "ker.erode.size", c.erode_ker_size },
			//{ "filter.a.low.color", { (int)c.color_low1[0], (int)c.color_low1[1], (int)c.color_low1[2] } },
			//{ "filter.a.high.color", { (int)c.color_high1[0], (int)c.color_high1[1], (int)c.color_high1[2] } },
			//{ "filter.b.low.color", { (int)c.color_low2[0], (int)c.color_low2[1], (int)c.color_low2[2] } },
			//{ "filter.b.high.color", { (int)c.color_high2[0], (int)c.color_high2[1], (int)c.color_high2[2] } }
		};

	}

	inline void from_json(const json_t & j, objdetect::config_t & c)
	{
		int buf;
		j.at("objects.max").get_to(c.max_objects);
		//j.at("ker.dilate.size").get_to(c.dilate_ker_size);
		//j.at("ker.erode.size").get_to(c.erode_ker_size);
		for (size_t i = 0; i < 3; ++i)
		{
		//	j.at("filter.a.low.color").at(i).get_to(buf); c.color_low1[i] = (float)buf;
		//	j.at("filter.a.high.color").at(i).get_to(buf); c.color_high1[i] = (float)buf;
		//	j.at("filter.b.low.color").at(i).get_to(buf); c.color_low2[i] = (float)buf;
		//	j.at("filter.b.high.color").at(i).get_to(buf); c.color_high2[i] = (float)buf;
		}

	}

}
