#pragma once

#include <nanopi/objdetect.hpp>

namespace nanopi {

/**
 * Predefined set of parameters for detection of
 * different types of objects.
 */
namespace objparams
{
    enum known_obj_t { KO_BALLOON, KO_LANDING_POS, _KO_MAX };

    template < known_obj_t _Obj >
    objdetect::config_t make_config();

    template <> inline objdetect::config_t make_config < KO_BALLOON > ()
    {
        objdetect::config_t cfg;
        cfg.max_objects = 3;
        cfg.color_low1  = {   0,  70,  50 };
        cfg.color_high1 = {  10, 255, 255 };
        cfg.color_low2  = { 170,  70,  50 };
        cfg.color_high2 = { 180, 255, 255 };
        cfg.erode_ker_size = 1;
        cfg.dilate_ker_size = 5;
        return cfg;
    }

    template <> inline objdetect::config_t make_config < KO_LANDING_POS > ()
    {
        objdetect::config_t cfg;
        cfg.max_objects = 1;
        cfg.color_low1  = { 0, 0, 0 };
        cfg.color_high1 = { 0, 0, 0 };
        cfg.color_low2  = { 0, 0, 0 };
        cfg.color_high2 = { 0, 0, 0 };
        cfg.erode_ker_size = 1;
        cfg.dilate_ker_size = 5;
        return cfg;
    }
};

}
