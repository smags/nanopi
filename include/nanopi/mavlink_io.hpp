#pragma once

#include <atomic>
#include <thread>

#include <nanopi/mavlink.hpp>
#include <nanopi/message_queue.hpp>
#include <nanopi/serial.hpp>

namespace nanopi {

/**
 * Background MAVLink receiver-transmitter.
 */
class mavlink_io
{
private:
    message_queue _in_queue;
    message_queue _out_queue;
    std::thread _worker;
    std::atomic_bool _working;
    serial _serial;
public:
    mavlink_io(serial && dev);
    ~mavlink_io()
    {
        stop();
    }
public:
    /**
     * Safely terminates IO without waiting
     * for all queues to become empty.
     */
    void stop()
    {
        if (valid())
        {
            _working = false;
            signal();
            _worker.join();
        }
    }
public:
    message_queue & get_input_queue() { return _in_queue; }
    message_queue & get_output_queue() { return _out_queue; }
public:
    bool valid() const { return _serial; }
    operator bool() const { return valid(); }
public:
    /**
     * Notifies all waiters.
     */
    void signal()
    {
        _in_queue.signal();
        _out_queue.signal();
    }
private:
    void loop();
};

}
