#pragma once

#include <nanopi/mavlink.hpp>

#define NANOPI_REC_MAX_OBJ 3
#define NANOPI_REC_MAX_BORDER_PTS 6

namespace nanopi {

/**
 * Internal abstract message definitions --
 * commands, responses to the, recognition results, etc.
 */
namespace messages {

struct msg_t
{
    uint64_t timestamp;
};

enum command_id_t : uint8_t
{
    COMMAND_ID_SET_CAMERA_STATE,
    COMMAND_ID_SET_CAMERA_IN_USE,
    COMMAND_ID_SET_CAMERA_STREAM_STATE
};

enum camera_state_t : uint8_t
{
    CAMERA_STATE_ON,
    CAMERA_STATE_OFF
};

enum camera_type_t : uint8_t
{
    CAMERA_TYPE_DOWN,
    CAMERA_TYPE_FORWARD
};

enum camera_stream_state_t : uint8_t
{
    CAMERA_STREAM_STATE_ON,
    CAMERA_STREAM_STATE_OFF
};

enum command_result_t : uint8_t
{
    RESULT_OK,
    RESULT_FAILURE
};

struct command_t : public msg_t
{
    command_id_t command;
    uint8_t param;
};

struct command_ack_t : public msg_t
{
    command_t command;
    command_result_t result;
};

enum obj_type_t : uint8_t
{
    OBJ_BALLOON,
    OBJ_LANDING_POS
};

struct point_t { float x, y; };

struct obj_t
{
    point_t center;
    uint8_t points;
    point_t border[NANOPI_REC_MAX_BORDER_PTS];
};

struct recognition_result_t : public msg_t
{
    obj_type_t type;
    uint8_t count;
    obj_t objects[NANOPI_REC_MAX_OBJ];
};

}

template <> inline bool pack(const messages::recognition_result_t & msg, mavlink_message_t & pack)
{
    mavlink_optical_recognition_t rec;

    rec.type = msg.type;
    rec.objects = msg.count;
    for (size_t i = 0; i < msg.count; ++i)
    {
        rec.center_x[i] = msg.objects[i].center.x;
        rec.center_y[i] = msg.objects[i].center.y;
        rec.points[i] = msg.objects[i].points;
        for (size_t j = 0; j < msg.objects[i].points; ++j)
        {
            rec.border_x[i * NANOPI_REC_MAX_OBJ + j] = msg.objects[i].border[j].x;
            rec.border_y[i * NANOPI_REC_MAX_OBJ + j] = msg.objects[i].border[j].y;
        }
    }

    mavlink_msg_optical_recognition_encode(
        NANOPI_MAV_SYS_ID, NANOPI_MAV_COMP_ID, &pack, &rec);

    return true;
}

inline bool decode_command(messages::command_t & cmd, uint16_t cmd_id, uint8_t cmd_param)
{
    // TODO: possibly switch to separate (nanopi-specific) commands
    switch (cmd_id)
    {
        // non-standard use: enable/disable processing
        case MAV_CMD_VIDEO_START_CAPTURE:
            cmd.command = messages::COMMAND_ID_SET_CAMERA_STATE;
            cmd.param = messages::CAMERA_STATE_ON;
            break;
        case MAV_CMD_VIDEO_STOP_CAPTURE:
            cmd.command = messages::COMMAND_ID_SET_CAMERA_STATE;
            cmd.param = messages::CAMERA_STATE_OFF;
            break;
        // non-standard use: switch current camera
        case MAV_CMD_SET_CAMERA_MODE:
            cmd.command = messages::COMMAND_ID_SET_CAMERA_IN_USE;
            cmd.param = cmd_param;
            break;
        // non-standard use: enable/disable custom streaming
        case MAV_CMD_VIDEO_START_STREAMING:
            cmd.command = messages::COMMAND_ID_SET_CAMERA_STREAM_STATE;
            cmd.param = messages::CAMERA_STREAM_STATE_ON;
            break;
        case MAV_CMD_VIDEO_STOP_STREAMING:
            cmd.command = messages::COMMAND_ID_SET_CAMERA_STREAM_STATE;
            cmd.param = messages::CAMERA_STREAM_STATE_OFF;
            break;
        default:
            return false;
    }

    return true;
}

inline bool encode_command(const messages::command_t & msg, uint16_t & cmd, uint8_t & param)
{
    switch (msg.command)
    {
        case messages::COMMAND_ID_SET_CAMERA_STATE:
            if (msg.param == messages::CAMERA_STATE_ON)
                cmd = MAV_CMD_VIDEO_START_CAPTURE;
            else cmd = MAV_CMD_VIDEO_STOP_CAPTURE;
            break;
        case messages::COMMAND_ID_SET_CAMERA_IN_USE:
            cmd = MAV_CMD_SET_CAMERA_MODE;
            param = msg.param;
            break;
        case messages::COMMAND_ID_SET_CAMERA_STREAM_STATE:
            if (msg.param == messages::CAMERA_STREAM_STATE_ON)
                cmd = MAV_CMD_VIDEO_START_STREAMING;
            else cmd = MAV_CMD_VIDEO_STOP_STREAMING;
            break;
        default:
            return false;
    }

    return true;
}

template <> inline bool pack(const messages::command_t & msg, mavlink_message_t & pack)
{
    mavlink_command_long_t cmd;
    uint16_t cmd_id;
    uint8_t param;

    if (!encode_command(msg, cmd_id, param)) return false;

    cmd.command = cmd_id;
    cmd.param1 = (float) param;

    mavlink_msg_command_long_encode(
        NANOPI_MAV_SYS_ID, NANOPI_MAV_COMP_ID, &pack, &cmd);

    return true;
}

template <> inline bool unpack(messages::command_t & msg, const mavlink_message_t & pack)
{
    if (pack.msgid != MAVLINK_MSG_ID_COMMAND_LONG) return false;

    mavlink_command_int_t cmd;
    mavlink_msg_command_int_decode(&pack, &cmd);

    return decode_command(msg, cmd.command, (uint8_t)round(cmd.param1));
}

template <> inline bool pack(const messages::command_ack_t & msg, mavlink_message_t & pack)
{
    mavlink_command_ack_t cmd;
    uint16_t cmd_id;
    uint8_t param;

    if (!encode_command(msg.command, cmd_id, param)) return false;

    cmd.command = cmd_id;

    mavlink_msg_command_ack_encode(
        NANOPI_MAV_SYS_ID, NANOPI_MAV_COMP_ID, &pack, &cmd);

    return true;
}

template <> inline bool unpack(messages::command_ack_t & msg, const mavlink_message_t & pack)
{
    if (pack.msgid != MAVLINK_MSG_ID_COMMAND_ACK) return false;

    mavlink_command_ack_t cmd;
    mavlink_msg_command_ack_decode(&pack, &cmd);

    msg.result = (messages::command_result_t)cmd.result;

    return decode_command(msg.command, cmd.command, (uint8_t)-1);
}

}
