﻿	nanopi::objdetect::config_t configuration;//конфигурация

	//configuration.mode = nanopi::RED_BALLOONS;
	configuration.mode = nanopi::WHITE_SQUARE;

	configuration.max_objects = 1;//Задаем колич., обнар. объектов

	//Задаем диапазон для обнаружения красных воздушных шаров.
	configuration.color_red_low = cv::Scalar(120, 100, 100);
	configuration.color_red_high = cv::Scalar(175, 255, 255);

	//Задаем диапазон для обнаружения белого квадрата (посадочн. площадки для дрона).
	configuration.color_white_low = cv::Scalar(140, 140, 140);
	configuration.color_white_high = cv::Scalar(255, 255, 255);

	//Задаем минимальную длину контура в пикс
	//configuration.min_contours_length=40;//ballons
	configuration.min_contours_length = 120;//white square


	//Параметры объектива камеры
	configuration.vert_angle = 45;
	configuration.horiz_angle = 45;

	//Параметры обнаруживаемых объектов
	configuration.red_sphere_max_size = 0.6;//Максимальный линейный размер красного воздушного шарика, м

	double square_dim = 1.0;//Длина стороны квадрата посадочной площадки (по регламенту 1,5 м), м
	configuration.white_square_max_size = sqrt(2*pow(square_dim, 2));//Максимальный линейный размер белого квадрата посадочной площадки, м


	nanopi::objdetect detector(configuration);

	cv::Mat mtx = cv::cvarrToMat(frame, true);//Current frame

	std::vector <nanopi::objdetect::obj_t> results; //Results of detection

	nanopi::objdetect::debug_t debug; //Debug info

	detector.detect(mtx, results, &debug); //Detection

	imshow("debug", debug.debug);
	waitKey(1);