# - Try to find  MAVLink
# Once done, this will define
#
#  MAVLINK_FOUND        : library found
#  MAVLINK_INCLUDE_DIRS : include directories
#  MAVLINK_VERSION      : version

# macros
include(FindPackageHandleStandardArgs)

set(_MAVLINK_EXTRA_SEARCH_HINTS
  ${CMAKE_SOURCE_DIR}/lib/mavlink/
)

# look for in the hints first
find_path(_MAVLINK_INCLUDE_DIR
  NAMES mavlink/v1.0/mavlink_types.h mavlink/v2.0/mavlink_types.h
  PATH_SUFFIXES include
  HINTS ${_MAVLINK_EXTRA_SEARCH_HINTS}
  NO_DEFAULT_PATH
)

set(MAVLINK_VERSION "2.0")

# handle arguments
set(MAVLINK_INCLUDE_DIRS ${_MAVLINK_INCLUDE_DIR})
find_package_handle_standard_args(
  MAVLink
  REQUIRED_VARS MAVLINK_INCLUDE_DIRS MAVLINK_VERSION
  VERSION_VAR MAVLINK_VERSION
)
