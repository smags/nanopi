#include <nanopi/message_queue.hpp>

#include <sys/eventfd.h>

namespace nanopi {

message_queue::message_queue()
    : _eventfd(eventfd(0, EFD_CLOEXEC))
{
}

message_queue::~message_queue()
{
    signal();
    close(_eventfd);
}

bool message_queue::push(const mavlink_message_t & msg)
{
    if (_queue.push(msg))
    {
        signal();
        return true;
    }
    return false;
}

bool message_queue::pop(mavlink_message_t & msg)
{
    return _queue.pop(msg);
}

void message_queue::signal()
{
    uint64_t u = 1;
    write(_eventfd, &u, sizeof(u));
}

}
