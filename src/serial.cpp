#include <nanopi/serial.hpp>

#include <sys/fcntl.h>
#include <sys/unistd.h>
#include <sys/termios.h>

namespace nanopi {

void serial::close()
{
    if (_fd)
    {
        ::close(_fd);
        _fd = 0;
    }
}

bool serial::open(params_t p)
{
    _fd = ::open(p.dev.c_str(), O_RDWR | O_NOCTTY);
    if (_fd < 0) { _fd = 0; return false; }
    if (!isatty(_fd)) { close(); return false; }
    struct termios port_cfg;
    if (tcgetattr(_fd, &port_cfg) < 0) { close(); return false; }
    cfmakeraw(&port_cfg);
    cfsetospeed(&port_cfg, p.baud_rate);
    cfsetispeed(&port_cfg, p.baud_rate);
    if (tcsetattr(_fd, TCSANOW, &port_cfg) < 0) { close(); return false; }
    return true;
}

}
