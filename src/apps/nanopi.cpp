#include <nanopi/video_capture.hpp>
#include <nanopi/objdetect.hpp>
#include <nanopi/objparams.hpp>
#include <nanopi/mavlink_io.hpp>
#include <nanopi/serial.hpp>
#include <nanopi/messages.hpp>
#include <nanopi/serialization.hpp>

#include <sys/signal.h>
#include <sys/unistd.h>
#include <sys/eventfd.h>
#include <sys/poll.h>
#include <sys/termios.h>

#include <atomic>
#include <fstream>

#include "config.hpp"

std::atomic_bool g_worikng { true };
int g_eventfd { eventfd(0, EFD_CLOEXEC) };

void sig_handler(int signo)
{
    // trap ctrl-c and ctrl-\ signals
    if (signo == SIGINT || signo == SIGQUIT) g_worikng = false;
    // signal all waiters
    uint64_t u = 1;
    ::write(g_eventfd, &u, sizeof(u));
}

int main(int argc, const char * argv[])
{
    if (argc > 2)
    {
        printf("usage: nanopi [config_path]\n");
        return 1;
    }

    std::string cfg_path = "config.json";
    if (argc == 2) cfg_path = argv[1];

    auto cfg = read_config(cfg_path);

    nanopi::serial::params_t serial_params;
    auto & reg = nanopi::video_capture::g_registry;
    nanopi::objdetect::config_t balloon_params, landing_params;

    get_serial_config(cfg, serial_params);
    get_capture_config(cfg, reg);
    get_detect_config(cfg, balloon_params, landing_params);

    nanopi::serial dev(serial_params);
    if (!dev) { printf("cannot open tty device\n"); return -1; }

    // discard any buffered data
    tcflush(dev, TCIOFLUSH);

    nanopi::mavlink_io io(std::move(dev));

    // open camera devices
    nanopi::video_capture cap_down(nanopi::video_capture::CAM_ID_DOWN);
    nanopi::video_capture cap_forward(nanopi::video_capture::CAM_ID_DOWN);

    if (!cap_down) { fprintf(stderr, "cannot open down camera\n"); return 1; }
    if (!cap_forward) { fprintf(stderr, "cannot open forward camera\n"); return 1; }

    // create two detectors for predefined types of objects
    nanopi::objdetect detect_balloon(balloon_params);
    nanopi::objdetect detect_land_pos(landing_params);

    // trap quit signals
    signal(SIGINT, sig_handler);
    signal(SIGQUIT, sig_handler);

    nanopi::messages::recognition_result_t msg_rec;
    nanopi::messages::command_t msg_cmd;
    nanopi::messages::command_ack_t msg_ack;
    mavlink_message_t mav_msg_cmd;

    pollfd fds[2];
    fds[0].fd = g_eventfd;
    fds[0].events = POLLIN;
    fds[1].fd = io.get_input_queue().get_eventfd();
    fds[1].events = POLLIN;

    nanopi::video_capture * cap = &cap_down;
    nanopi::objdetect * detect = &detect_land_pos;
    cv::Mat buf;
    std::vector < nanopi::objdetect::obj_t > objects;

    bool enabled = false;

    // main loop
    // if waits for commands from 'external world',
    // answers them, performs actual object detection
    while (g_worikng)
    {
        // if enabled, use current camera and current
        // detector to capture and detect objects
        // and send recognition results back
        if (enabled)
        {
            if (cap->capture(buf))
            {
                detect->detect(buf, objects);
                msg_rec.count = objects.size();
                msg_rec.type = (detect == &detect_balloon)
                    ? nanopi::messages::obj_type_t::OBJ_BALLOON
                    : nanopi::messages::obj_type_t::OBJ_LANDING_POS;
                for (size_t i = 0; i < objects.size(); ++i)
                {
                    // TODO: update objdetect
                    msg_rec.objects[i].center.x = objects[i].cx;
                    msg_rec.objects[i].center.y = objects[i].cy;
                }
                io.get_output_queue().push(msg_rec);
            }
        }
        else
        {
            // wait for either external message or termination request
            int ret = ::poll(fds, 2, -1);
            if (ret > 0)
            {
                // reset event
                uint64_t u;
                if (fds[0].revents & POLLIN)
                    ::read(fds[0].fd, &u, sizeof(u));
                if (fds[1].revents & POLLIN)
                    ::read(fds[1].fd, &u, sizeof(u));
            }
        }

        // process all received messages while dropping all unknown ones
        while (io.get_input_queue().pop(mav_msg_cmd))
        {
            // expect commands only
            if (nanopi::unpack(msg_cmd, mav_msg_cmd))
            {
                bool ack = false;
                switch (msg_cmd.command)
                {
                    case nanopi::messages::COMMAND_ID_SET_CAMERA_STATE:
                        if (msg_cmd.param == nanopi::messages::CAMERA_STATE_ON) enabled = true;
                        else enabled = false;
                        ack = true;
                        break;
                    case nanopi::messages::COMMAND_ID_SET_CAMERA_STREAM_STATE:
                        // TODO: integrate streaming
                        break;
                    case nanopi::messages::COMMAND_ID_SET_CAMERA_IN_USE:
                        if (msg_cmd.param == nanopi::messages::CAMERA_TYPE_DOWN)
                        {
                            cap = &cap_down;
                            detect = &detect_land_pos;
                        }
                        else
                        {
                            cap = &cap_forward;
                            detect = &detect_balloon;
                        }
                        ack = true;
                        break;
                }
                msg_ack.command = msg_cmd;
                msg_ack.result = ack ? nanopi::messages::RESULT_OK : nanopi::messages::RESULT_FAILURE;
                io.get_output_queue().push(msg_ack);
            }
        }
    }

    fprintf(stderr, "stopped\n");

    return 0;
}
