#include <nanopi/mavlink_io.hpp>
#include <nanopi/serial.hpp>
#include <nanopi/messages.hpp>

#include <sys/signal.h>
#include <sys/unistd.h>
#include <sys/eventfd.h>
#include <sys/poll.h>
#include <sys/termios.h>
#include <sys/time.h>

/**
 * Sends commands to serial port and waits for acks.
 */

void usage()
{
    printf("usage: <tty_dev> <baud> <command>\n"
            "  <command> = enable|disable|start|stop|down|front\n");
}

int main(int argc, const char * argv[])
{
    if (argc < 4) { usage(); return 1; }

    nanopi::messages::command_t msg_cmd;
    nanopi::messages::command_ack_t msg_ack;
    mavlink_message_t mav_msg;

    if (strcmp(argv[3], "enable") == 0)
    {
        msg_cmd.command = nanopi::messages::COMMAND_ID_SET_CAMERA_STATE;
        msg_cmd.param = nanopi::messages::CAMERA_STATE_ON;
    }
    else if (strcmp(argv[3], "disable") == 0)
    {
        msg_cmd.command = nanopi::messages::COMMAND_ID_SET_CAMERA_STATE;
        msg_cmd.param = nanopi::messages::CAMERA_STATE_OFF;
    }
    else if (strcmp(argv[3], "start") == 0)
    {
        msg_cmd.command = nanopi::messages::COMMAND_ID_SET_CAMERA_STREAM_STATE;
        msg_cmd.param = nanopi::messages::CAMERA_STREAM_STATE_ON;
    }
    else if (strcmp(argv[3], "stop") == 0)
    {
        msg_cmd.command = nanopi::messages::COMMAND_ID_SET_CAMERA_STREAM_STATE;
        msg_cmd.param = nanopi::messages::CAMERA_STREAM_STATE_OFF;
    }
    else if (strcmp(argv[3], "down") == 0)
    {
        msg_cmd.command = nanopi::messages::COMMAND_ID_SET_CAMERA_IN_USE;
        msg_cmd.param = nanopi::messages::CAMERA_TYPE_DOWN;
    }
    else if (strcmp(argv[3], "forward") == 0)
    {
        msg_cmd.command = nanopi::messages::COMMAND_ID_SET_CAMERA_IN_USE;
        msg_cmd.param = nanopi::messages::CAMERA_TYPE_FORWARD;
    }
    else
    {
        usage();
        return 1;
    }

    nanopi::serial dev({ argv[1], atoi(argv[2]) });
    if (!dev) { printf("cannot open tty device\n"); return -1; }

    // discard any buffered data
    tcflush(dev, TCIOFLUSH);

    nanopi::mavlink_io io(std::move(dev));

    io.get_output_queue().push(msg_cmd);

    pollfd fds[1];
    fds[0].fd = io.get_input_queue().get_eventfd();
    fds[0].events = POLLIN;

    struct timeval then, now;
    gettimeofday(&then, NULL);

    bool ack = false;
    for (;;)
    {
        ::poll(fds, 1, 1000);
        while (io.get_input_queue().pop(mav_msg))
        {
            if (nanopi::unpack(msg_ack, mav_msg))
            {
                if (msg_ack.command.command == msg_cmd.command)
                {
                    fprintf(stderr, "ack received\n");
                    return 0;
                }
            }
        }
        gettimeofday(&now, NULL);
        if (now.tv_sec - then.tv_sec > 3) break;
    }

    fprintf(stderr, "no ack received\n");
    return 1;
}
