#include <nanopi/message_queue.hpp>
#include <nanopi/messages.hpp>
#include <nanopi/mavlink_io.hpp>

#include <sys/termios.h>
#include <sys/signal.h>

#include <atomic>

std::atomic_bool g_worikng { true };

void sig_handler(int signo)
{
    // trap ctrl-c and ctrl-\ signals
    if (signo == SIGINT || signo == SIGQUIT) g_worikng = false;
}

int main(int argc, const char * argv[])
{
    if (argc < 3) { printf("usage: <port> <baud>\n"); return 1; }

    nanopi::serial dev({ argv[1], atoi(argv[2]) });
    if (!dev) { printf("cannot open device\n"); return -1; }

    // discard any buffered data
    tcflush(dev, TCIOFLUSH);

    // sample message
    mavlink_message_t msg;
    mavlink_msg_command_ack_pack(
        0,                      // system id (may be 0)
        0,                      // component id (may be 0)
        &msg,                   // generic buffer struct
        123, 1, 0, 456, 0, 0    // message-specific params
    );

    // trap quit signals
    signal(SIGINT, sig_handler);
    signal(SIGQUIT, sig_handler);

    {
        nanopi::mavlink_io io(std::move(dev));

        mavlink_message_t buf;
        while (g_worikng)
        {
            if (io.get_output_queue().push(msg)) fprintf(stderr, ". ");
            while (io.get_input_queue().pop(buf))
                fprintf(stderr, "\nreceived: %d\n", buf.msgid);
            sleep(1);
        }

        fprintf(stderr, "\nshutting down\n");
    }

    fprintf(stderr, "stopped\n");

    return 0;
}
