#pragma once

#include <nanopi/video_capture.hpp>
#include <nanopi/objdetect.hpp>
#include <nanopi/objparams.hpp>
#include <nanopi/serial.hpp>
#include <nanopi/serialization.hpp>

#include <fstream>

inline json_t make_default_config()
{
    return
    {
        { "serial", nanopi::serial::params_t { "/dev/ttyS0", 9600 } },
        { "objdetect", {
            { "config.balloon", "balloon" },
            { "config.landing", "landing" },
            { "configs", {
                { "balloon", nanopi::objparams::make_config < nanopi::objparams::KO_BALLOON > () },
                { "landing", nanopi::objparams::make_config < nanopi::objparams::KO_LANDING_POS > () }
            } }
        } },
        { "capture", {
            { "cam.down", "down" },
            { "cam.forward", "forward" },
            { "cams", {
                { "down", nanopi::video_capture::g_registry.cams[nanopi::video_capture::CAM_ID_DOWN] },
                { "forward", nanopi::video_capture::g_registry.cams[nanopi::video_capture::CAM_ID_FORWARD] }
            } }
        } }
    };
}

inline json_t read_config(std::string path)
{
    std::ifstream in(path);
    json_t res = {};
    if (in)
    {
        try
        {
            res = json_t::parse(std::move(in));
        }
        catch (std::exception ex)
        {
            std::cerr << ex.what() << std::endl;
            return res;
        }
    }
    if (res.empty())
    {
        res = make_default_config();
        std::ofstream o(path);
        o << res.dump(4) << std::endl;
    }
    return res;
}

inline void get_serial_config(const json_t & c, nanopi::serial::params_t & p)
{
    c["serial"].get_to(p);
}

inline void get_detect_config(const json_t & c,
    std::string name,
    nanopi::objdetect::config_t & cfg)
{
    c["objdetect"]["configs"][name].get_to(cfg);
}

inline void get_detect_config(const json_t & c,
    nanopi::objdetect::config_t & balloon,
    nanopi::objdetect::config_t & landing)
{
    get_detect_config(c, c["objdetect"]["config.balloon"], balloon);
    get_detect_config(c, c["objdetect"]["config.landing"], balloon);
}

inline void get_capture_config(const json_t & c,
    nanopi::video_capture::registry_t & reg)
{
    auto & root = c["capture"];
    root["cams"][root["cam.down"].get < std::string > ()].get_to(reg.cams[nanopi::video_capture::CAM_ID_DOWN]);
    root["cams"][root["cam.forward"].get < std::string > ()].get_to(reg.cams[nanopi::video_capture::CAM_ID_FORWARD]);
}
