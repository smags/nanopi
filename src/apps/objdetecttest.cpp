#include <nanopi/video_capture.hpp>
#include <nanopi/objdetect.hpp>
#include <nanopi/objparams.hpp>

#include <sys/signal.h>
#include <sys/unistd.h>

#include <atomic>

#include "config.hpp"

std::atomic_bool g_worikng { true };

void sig_handler(int signo)
{
    // trap ctrl-c and ctrl-\ signals
    if (signo == SIGINT || signo == SIGQUIT) g_worikng = false;
}

int main(int argc, const char * argv[])
{
    if (argc > 3)
    {
        printf("usage: objdetect [profile] [config_path]\n");
        return 1;
    }

    std::string profile = "balloon";
    if (argc == 2) profile = argv[1];

    std::string cfg_path = "config.json";
    if (argc == 3) cfg_path = argv[2];

    auto cfg = read_config(cfg_path);

    auto & reg = nanopi::video_capture::g_registry;
    nanopi::objdetect::config_t rec_params;

    get_capture_config(cfg, reg);
    get_detect_config(cfg, profile, rec_params);

    // open device
    nanopi::video_capture cap(nanopi::video_capture::CAM_ID_DOWN);

    if (!cap) { fprintf(stderr, "cannot open camera\n"); return 1; }

    // create detector of predefined type of objects
    nanopi::objdetect detect(rec_params);

    // trap quit signals
    signal(SIGINT, sig_handler);
    signal(SIGQUIT, sig_handler);

    cv::Mat buf;
    std::vector < nanopi::objdetect::obj_t > objects;
    nanopi::objdetect::debug_t debug;

    while (g_worikng)
    {
        if (!cap.capture(buf)) fprintf(stderr, "!captured\n");
        else
        {
            detect.detect(buf, objects, &debug);

            // display debug graphics
            cv::Mat mask, two;
            cv::cvtColor(debug.mask, mask, cv::COLOR_GRAY2RGB);
            cv::hconcat(debug.debug, mask, two);
            cv::imshow("debug", two);
            if (cv::waitKey(25) >= 0) break;
        }
    }

    cv::destroyAllWindows();

    fprintf(stderr, "stopped\n");

    return 0;
}
