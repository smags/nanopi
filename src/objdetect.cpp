//**************************************************************
//�����������:          ����������� ������������, ���. ���� �����. ����������, �. ������ ��������    
//������:			    ��� ���							
//������ �������:       1.0                                  
//������������:         ANSI C++ 98                         
//������:   		    28.05.2019
//�������:   		    29.05.2019
//**************************************************************//

/** \author ������ �.�.*/
/** \file objdetect.cpp*/
/** \brief ���������� ��������� ����������� ��������� ����� � ���������� �������� ����� ��� ������� �����������.*/


#include "nanopi/objdetect.hpp"

#define SPACE_RESOLUTION 5 //<! Size of median filter.

#define MIN_COLOR_VALUE 0 //<! Minimal value of color channel.
#define MAX_COLOR_VALUE 255 //<! Maximal value of color channel.

#define CONTOUR_THICKNESS 3 //<! Drawed contour thickness.

#define RGB_COLOR_MODEL 3 //<! Channels of RGB model.

#define CANNY_LOW_THRESHOLD 100 //<! Canny edge detector low threshold parameter.
#define CANNY_HIGH_THRESHOLD 255 //<! Canny edge detector high threshold parameter.
#define CANNY_KERNEL_SIZE 3 //<! Canny edge detector kernel size.

#define DILATION_KERNEL_SIZE 5 //<! Dilation kernel size.

#define WHITE_SQUARE_IDEAL_SIZES_DIFF 0.3 //<! Max differences from ideal for white square dimension ratios.
#define MAX_STDEV_FOR_WHITE_SQUARE 105 //<! Max stdev value for white zone.

namespace nanopi
{


	void Dilation(int dilation_elem, int dilation_size, cv::_InputArray src, cv::_OutputArray dst)
	{
		int dilation_type;
		if (dilation_elem == 0) { dilation_type = cv::MORPH_RECT; }
		else if (dilation_elem == 1) { dilation_type = cv::MORPH_CROSS; }
		else if (dilation_elem == 2) { dilation_type = cv::MORPH_ELLIPSE; }

		cv::Mat element = cv::getStructuringElement(dilation_type,
			cv::Size(2 * dilation_size + 1, 2 * dilation_size + 1),
			cv::Point(dilation_size, dilation_size));

		/// Apply the dilation operation
		dilate(src, dst, element);
	}

	objdetect::objdetect(config_t cfg)
		: _config(cfg)//
	{

	}

	bool objdetect::detect_red_balloons(const cv::Mat & frame, std::vector < obj_t > & res, debug_t * debug)
	{
		bool result_of_detection = false;

		int channels = frame.channels();

		if (channels == 3)
		{
			cvtColor(frame, _hsv, cv::COLOR_BGR2HSV);//Conversion to HSV
		}
		else if (channels == 4)
		{
			cvtColor(frame, _hsv, cv::COLOR_BGRA2RGB);//Conversion to RGB
			cvtColor(_hsv, _hsv, cv::COLOR_RGB2HSV);//Conversion to HSV
		}
		else throw std::invalid_argument("The captured image hasn't 3 or 4 color channels!");


		//Select colored objects
		inRange(_hsv, _config.color_red_low, _config.color_red_high, _mask);
		
		if (debug != 0)debug->mask = _mask.clone();

		//Blurring
		cv::medianBlur(_mask, _mask, SPACE_RESOLUTION);

		//Finding of contours

		std::vector<std::vector<cv::Point>> contours;
		cv::findContours(_mask, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);

		int quantity_detected_objects = 0;

		//Filtering of contours by size, shape and others
		for (std::vector<std::vector<cv::Point>>::const_iterator it(contours.begin()); it != contours.end(); ++it)
		{
			size_t contour_length = (*it).size();//Contour length

			if (contour_length <= _config.min_contours_length)continue;//Filtering of contour length

			std::vector<cv::Point> hull;//arounding hull
			cv::convexHull(*it, hull);

			obj_t detected_object;
			detected_object.hull = hull;

			cv::Point2f center;
			float radius;

			cv::minEnclosingCircle(hull, center, radius);

			double object_area = cv::contourArea(hull);
			double theory_object_area = CV_PI * radius*radius;

			double circle_ratio = theory_object_area / object_area;

			if (circle_ratio>1.4)continue;

			quantity_detected_objects++;

			detected_object.cx = center.x*_config.horiz_angle / frame.cols - _config.horiz_angle / 2;
			detected_object.cy = center.y*_config.vert_angle / frame.rows - _config.vert_angle / 2;

			detected_object.size = radius * 2 * _config.horiz_angle / frame.cols;

			double object_size;

			detected_object.distance = (_config.red_sphere_max_size / 2)*tan(detected_object.size / 2);
			detected_object.object_type = nanopi::RED_BALLOONS;

			
			//Drawing of detected contours

			if (debug != 0)
			{
				cv::Mat drawed_mtx = frame.clone();

				int channels = drawed_mtx.channels();

				if (channels == 3)cvtColor(drawed_mtx, drawed_mtx, cv::COLOR_BGR2RGB);//Conversion to RGB
				else cvtColor(drawed_mtx, drawed_mtx, cv::COLOR_BGRA2RGB);//Conversion to RGB

				std::vector<std::vector<cv::Point>> contour_envelop;

				contour_envelop.push_back(hull);

				cv::drawContours(drawed_mtx, contour_envelop, -1, cv::Scalar(MIN_COLOR_VALUE, MIN_COLOR_VALUE, MAX_COLOR_VALUE), CONTOUR_THICKNESS);
				debug->debug = drawed_mtx.clone();
			}

			res.push_back(detected_object);
			result_of_detection = true;

			if (quantity_detected_objects == _config.max_objects)break;
		}

		return result_of_detection;
	}

	
	bool objdetect::detect_white_square(const cv::Mat & frame, std::vector < obj_t > & res, debug_t * debug)
	{
		bool result_of_detection = false;

		int channels = frame.channels();

		if (channels == 3)
		{
			cvtColor(frame, _hsv, cv::COLOR_BGR2RGB);//Conversion to RGB
		}
		else if (channels == 4)
		{
			cvtColor(frame, _hsv, cv::COLOR_BGRA2RGB);//Conversion to RGB

		}
		else throw std::invalid_argument("The captured image hasn't 3 or 4 color channels!");

		cv::Mat frame_gray;//Gray image

		/// Convert the image to grayscale
		cv::cvtColor(_hsv, frame_gray, CV_RGB2GRAY);

		/// Reduce noise with a kernel 3x3
		//cv::blur(frame_gray, frame_gray, cv::Size(5, 5));
		cv::medianBlur(frame_gray, frame_gray, SPACE_RESOLUTION);

		/// Create a matrix of the same type and size as src (for dst)
		cv::Mat dst;
		dst.create(_hsv.size(), _hsv.type());

		/// Canny detector
		Canny(frame_gray, _detected_edges, CANNY_LOW_THRESHOLD, CANNY_HIGH_THRESHOLD, CANNY_KERNEL_SIZE);

		//Contors closuring
		Dilation(0, DILATION_KERNEL_SIZE, _detected_edges, _detected_edges);

		//cv::Mat tmp = _detected_edges.clone();

		std::vector<std::vector<cv::Point>> contours;
		cv::findContours(_detected_edges, contours, CV_RETR_LIST, CV_CHAIN_APPROX_NONE);

		int max_area_of_object = 0;

		bool result_of_det = false;

		obj_t detected_object;

		//Filtering of contours by size, shape and others
		for (std::vector<std::vector<cv::Point>>::const_iterator it(contours.begin()); it != contours.end(); ++it)
		{
			size_t contour_length = (*it).size();//Contour length

			if (contour_length <= _config.min_contours_length)continue;//Filtering of contour length

			cv::RotatedRect ellipse=cv::minAreaRect(*it);

			double ellipse_area = ellipse.size.area();

			double contour_area=cv::contourArea(*it);

			if((ellipse.size.width/ellipse.size.height > 1+ WHITE_SQUARE_IDEAL_SIZES_DIFF || ellipse.size.width/ellipse.size.height < 1 - WHITE_SQUARE_IDEAL_SIZES_DIFF) ||
			   (ellipse_area/contour_area > 1+ WHITE_SQUARE_IDEAL_SIZES_DIFF || ellipse.size.width/ellipse.size.height < 1- WHITE_SQUARE_IDEAL_SIZES_DIFF)) continue;

			//Computing mean and stdev for area inner contours
			cv::Mat _hsv_roi = _hsv(ellipse.boundingRect());
			cv::Scalar roi_mean;
			cv::Scalar roi_stdev;
			cv::meanStdDev(_hsv_roi, roi_mean, roi_stdev);

			if (roi_mean[0] < _config.color_white_low[0] ||
				roi_mean[1] < _config.color_white_low[1] ||
				roi_mean[2] < _config.color_white_low[2] ||
				roi_stdev[0] > MAX_STDEV_FOR_WHITE_SQUARE ||
				roi_stdev[1] > MAX_STDEV_FOR_WHITE_SQUARE ||
				roi_stdev[2] > MAX_STDEV_FOR_WHITE_SQUARE) continue;

			if (contour_area > max_area_of_object)
			{
				max_area_of_object = contour_area;
				
				cv::Point2f hull[4];
				ellipse.points(hull);

				detected_object.hull.push_back(cv::Point(hull[0].x, hull[0].y));
				detected_object.hull.push_back(cv::Point(hull[1].x, hull[1].y));
				detected_object.hull.push_back(cv::Point(hull[2].x, hull[2].y));
				detected_object.hull.push_back(cv::Point(hull[3].x, hull[3].y));

				detected_object.cx = ellipse.center.x;
				detected_object.cy = ellipse.center.y;

				detected_object.size = sqrt(pow(ellipse.size.width, 2)+pow(ellipse.size.height, 2)) * _config.horiz_angle / frame.cols;

				double object_size;

				detected_object.distance = (_config.white_square_max_size / 2)*tan(detected_object.size / 2);
				detected_object.object_type = nanopi::WHITE_SQUARE;


				result_of_det = true;
			}

			//Drawing of detected contours

			if (debug != 0)
			{
				cv::Mat drawed_mtx = frame.clone();

				int channels = drawed_mtx.channels();

				if (channels == 3)cvtColor(drawed_mtx, drawed_mtx, cv::COLOR_BGR2RGB);//Conversion to RGB
				else cvtColor(drawed_mtx, drawed_mtx, cv::COLOR_BGRA2RGB);//Conversion to RGB

				std::vector<std::vector<cv::Point>> contour_envelop;

				contour_envelop.push_back(*it);

				cv::drawContours(drawed_mtx, contour_envelop, -1, cv::Scalar(MIN_COLOR_VALUE, MIN_COLOR_VALUE, MAX_COLOR_VALUE), CONTOUR_THICKNESS);
				debug->debug = drawed_mtx.clone();

				//cv::imshow("debug", debug->debug);
				//cv::waitKey(1);

				//cv::imshow("edges", tmp);
				//cv::waitKey(1);
			}
		}

		//cv::imshow("debug", _detected_edges);
		//cv::waitKey(1);
		if (result_of_detection == true)res.push_back(detected_object);

		return result_of_det;
	}


	bool objdetect::detect(const cv::Mat & frame, std::vector < obj_t > & res, debug_t * debug)
	{
		if (_config.mode == nanopi::RED_BALLOONS) return detect_red_balloons(frame, res, debug);
		else return detect_white_square(frame, res, debug);
	}

}
