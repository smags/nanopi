#include <nanopi/mavlink_io.hpp>

#include <sys/eventfd.h>
#include <sys/poll.h>
#include <sys/fcntl.h>
#include <sys/unistd.h>
#include <sys/termios.h>

namespace nanopi {

mavlink_io::mavlink_io(serial && dev)
{
    if (dev)
    {
        _serial = std::move(dev);
        _working = true;
        _worker = std::thread(&mavlink_io::loop, this);
    }
    else
    {
        _working = false;
    }
}

void mavlink_io::loop()
{
    enum : size_t { FDS_SERIAL, FDS_QUEUE, _FDS_SIZE };
    struct pollfd fds[_FDS_SIZE];
    fds[FDS_SERIAL].fd = _serial;
    fds[FDS_SERIAL].events = POLLIN;
    fds[FDS_QUEUE].fd = _out_queue.get_eventfd();
    fds[FDS_QUEUE].events = POLLIN;

    uint8_t buf[65535];
    mavlink_message_t msg;
    mavlink_status_t status;

    for(;;)
    {
        int ret = ::poll(fds, _FDS_SIZE, -1);

        if (ret < 0) continue;

        if (!_working) break;

        if (fds[FDS_SERIAL].revents & POLLIN)
        {
            int len = ::read(_serial, buf, sizeof(buf));
            if (len > 0) {
                for (unsigned i = 0; i < len; ++i)
                {
                    if (mavlink_parse_char(0, buf[i], &msg, &status))
                    {
                        _in_queue.push(msg);
                    }
                }
            }
        }

        if (fds[FDS_QUEUE].revents & POLLIN)
        {
            // reset event
            uint64_t u;
            ::read(_out_queue.get_eventfd(), &u, sizeof(u));

            while (_out_queue.pop(msg))
            {
                mavlink_msg_to_send_buffer(buf, &msg);
                size_t len = mavlink_msg_get_send_buffer_length(&msg);
                int ret = ::write(_serial, buf, len);
                if (ret < len)
                {
                    // TODO: record failure, analyze failure freq
                    // possibly support partial write
                }
            }
        }
    }
}

}
