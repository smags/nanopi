# nanopi

Part of the "ULET" UAV system.

A set of libraries and tools to use on the companion computer (NanoPi NEO with Ubuntu Core 16.04 LTS).


## How to Build

Just use `cmake`:

```bash
git clone --recurse-submodules https://bitbucket.org/smags/nanopi.git
cd nanopi
mkdir build
cmake .. && make
```


# Prerequirements

```bash
list=$(apt-cache --names-only search ^gstreamer1.0-* | awk '{ print $1 }' | grep -v gstreamer1.0-hybris)
sudo apt install $list

sudo apt install libgstreamer-plugins-base1.0-dev

sudo apt install libboost-all-dev
```
